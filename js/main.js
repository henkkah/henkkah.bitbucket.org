var image = new Image();
image.onload = function () {
    console.info("Image loaded !");
    var initDelay = 2100;
    var logo = $(".Logo");
    var greeting = $(".Greeting");
    var footerText = $(".Footer__text p");
    var curtain = $(".Curtain");
    var icicleGlows = $("#icicleglow").find("path");
    var windowGlows = $("#windowGlow").children();
    var roundWindowGlows1 = $("#windowGlowRound1").find("path");
    var roundWindowGlows2 = $("#windowGlowRound2").find("path");
    var starsTop = $("#starsTop").find("g");
    var starsMiddle = $("#starsMiddle").find("g");
    var starsBottom = $("#starsBottom").find("g");
    var smoke = $("#smoke").find("path");
    var window = $("#window").find("path").find("polygon");
    var flakeCount = 250;
    var minSize = 5;
    var maxSize = 20;
    var moonY = -435;
    var moonX = moonY * -2;
    var moonTime = 90000;

    if (Modernizr.mq("screen and (max-width:1023px)")) {
        flakeCount = 150;
        minSize = 3;
        maxSize = 10;
        moonY = -70;
        moonX = 270;
    }
    if (Modernizr.mq("screen and (min-width: 1024px) and (max-width: 1679px)")) {
        flakeCount = 200;
        minSize = 3;
        maxSize = 12;
        moonX = 580;
        moonY = -52;
    }

    initOpacity(icicleGlows);
    initOpacity(windowGlows);
    initOpacity(roundWindowGlows1);
    initOpacity(roundWindowGlows2);
    initOpacity(starsTop);
    initOpacity(starsMiddle);
    initOpacity(starsBottom);
    initOpacity(smoke);

    icicleGlows.each(function (i, e) {
        windowBlink($(e));
    });
    starsTop.each(function (i, e) {
        starBlink($(e));
    });
    starsMiddle.each(function (i, e) {
        starBlink($(e));
    });
    starsBottom.each(function (i, e) {
        starBlink($(e));
    });
    smoke.each(function (i, e) {
        animateSmoke($(e));
    });
    windowGlows.each(function (i, e) {
        windowBlink($(e));
    });
    roundWindowGlows1.each(function (i, e) {
        windowBlink($(e));
    });
    roundWindowGlows2.each(function (i, e) {
        windowBlink($(e));
    });

    function initOpacity(elems) {
        elems.each(function (i, e) {
            var elem = $(e);
            if (elem.attr("opacity")) {
                elem.attr("data-opacity", elem.attr("opacity")).attr("opacity", 0);
            } else {
                elem.attr("data-opacity", elem.attr("opacity")).attr("opacity", 0);
            }
        });
    }

    function windowBlink(e){
        var delay = Math.floor(Math.random() * 1000);
        var opacity = $(e).attr("data-opacity") || 1;
        $(e).delay(delay).animate({
            opacity: opacity
        }, 600).animate({
            opacity: opacity - .16
        }, 600, function() {
            windowBlink(e);
        });
    }

    function starBlink(e){
        var delay = Math.floor(Math.random() * 3000);
        $(e).delay(delay).animate({
            opacity: 1
        }, 600, function() {
        }).animate({
            opacity: 0
        }, 600, function() {
            starBlink(e);
        });
    }

    function animateSmoke(e){
        var delay = Math.floor(Math.random() * 5000);
        var opacity = $(e).attr("data-opacity") || 1;
        $(e).delay(delay).animate({
            opacity: opacity
        }, 3000, function() {
            // Animation complete.
        }).animate({
            opacity: 0
        }, 3000, function() {
            animateSmoke(e);
        });
    }

    $('.Animation__Moon').transition({ x: moonX, y: moonY }, moonTime, 'linear');

    function startSnowFall() {
        $('.Snowfall').snowfall('clear').snowfall({image: "img/snowflake.svg", minSize: minSize, maxSize: maxSize, deviceorientation: true, flakeCount: flakeCount, minSpeed: 3, maxSpeed: 5});
        $.fn.center = function () { // TODO See if this is still needed
            this.css("position", "absolute");
            this.css("top", Math.max(0, (($(window).height()
                    - $(this).outerHeight()) / 2) +
                    $(window).scrollTop()) + "px");
            this.css("left", Math.max(0, (($(window).width()
                    - $(this).outerWidth()) / 2) +
                    $(window).scrollLeft()) + "px");
            return this;
        }
    }

    $(document).ready(function () {
        startSnowFall();

        // Animate
        curtain.fadeOut(2100)
        logo.delay(initDelay).fadeIn(800);
        greeting.delay(initDelay + 3000).fadeIn(800);
        footerText.delay(initDelay + 7000).fadeIn(3000);
    });

    $(window).resize(function () {
        startSnowFall();
    });
};
image.onerror = function () {
    console.error("Cannot load image");
};
image.src = "img/bg.png";